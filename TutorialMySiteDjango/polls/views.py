from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse,Http404, HttpResponseRedirect
from .models import Choice, Question
from django.template import loader
from django.urls import reverse
from django.views import generic
from django.utils import timezone

# Create your views here.
# One feature of the views. Django wants only HttpResponse or an Exception!!
# Here we define the URLS
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    # This function obtain data to store into context_object_name
    def get_queryset(self):
    # We list last five question by pub_date parameter
    # lte stands for Less Than or Equal operator. It is used in filter method
        return Question.objects.filter(pub_date__lte = timezone.now()).order_by('-pub_date')[:5]

# Now we define some new views for different tasks

# model is an attribute of generic class which permits to generalize commands
class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
    # We list last five question by pub_date parameter
    # lte stands for Less Than or Equal operator. It is used in filter method
        return Question.objects.filter(pub_date__lte = timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

def vote(request, question_id):
    # Another way to fecth data from db is to use the following function
    # which handles also 404 errors.
    question = get_object_or_404(Question,pk=question_id)
    try:
        # request.POST is a list which contains user input (Good Practice!)
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
        # It raises KeyError if choice field does not exists
    except (KeyError, Choice.DoesNotExist):
        return render(request,'polls/detail.html', {'question': question,
                                                   'error_message': 'You did not select a valid choice.'},)
    else:
        selected_choice.votes += 1
        selected_choice.save() # Modify Entry in DB
    # Redirection of the user to the same page!
    # We use reverse in order not to hard-code the correct page
    # we can still use response % question_id but this is much more readable
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
