from django.contrib import admin
from .models import Choice, Question
# Register your models here.
class ChoiceInline(admin.TabularInline):
    # This says that Choice objects are edited on the Question admin page
    # Add 3 empty slots for each Question every time we select a Question
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    # This is a simple implementation without spanning and adding a title
    #fields = ['pub_date' , 'question_text']
    list_display = ('question_text', 'pub_date', 'was_published_recently') # We change the visualization
    fieldsets = [
    (        "Question",       {'fields':['question_text']}),
    ('Date Information', {'fields':['pub_date'], 'classes':['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_filter = ['pub_date'] # In order to have a panel on the right of the site for filtering entries
    search_fields = ['question_text'] # In order to have an input box for searching questions

admin.site.register(Question, QuestionAdmin)

# If we want to have an admin interface for certain models
# we have to define here so they can be modified also in the admin panel in the browser
