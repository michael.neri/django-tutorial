import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse


from .models import Question
# Create your tests here. Testing is a fundamental process of developing an app
# Here we can define some cases

## Tests related to Models we implemented

class QuestionModelTests(TestCase):
    # Its name must begin with test
    def test_was_published_recently_with_future_question(self):
        #
        time =  timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        # Test says that a Question in the future in not "recent" so it ought to be false!
        # It raises an AssertionError if something is not correct!
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        #
        time = timezone.now() - datetime.timedelta(days =1, seconds=1)
        old_question = Question(pub_date = time)
        # Test says that a Question created more than 1 day of life is not "recent" so it ought to be false!
        # It raises an AssertionError if something is not correct!
        self.assertIs(old_question.was_published_recently(),False)

    def test_was_published_recently_with_recent_question(self):
        #
        time = timezone.now() - datetime.timedelta(hours = 23, minutes = 59, seconds = 59)
        recent_question = Question(pub_date = time)
                # Test says that a Question less than 1 day of life in  "recent" so it ought to be true!
                # It raises an AssertionError if something is not correct!
        self.assertIs(recent_question.was_published_recently(), True)

def create_question(question_text, days):
        # Creates a question with an offset of days (+ for future - for past)
        time = timezone.now() + datetime.timedelta(days = days)
        return Question.objects.create(question_text = question_text, pub_date = time)
####################
# !!!! Important !!!!
# We have to remember that we create a DB from zero so we test entries in order not to have bug
###########

# Tests related to Index view
class QuestionIndexViewTests(TestCase):
    def test_no_question(self):
        # Check if no question exists, an appropriate message is displayed
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        # Question with a pub_date in the past must be displayed in the Index
        create_question(question_text="Past Question.", days = -30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past Question.>'])

    def test_future_question(self):
        # Question with future pub_date must not be displayed in the index
        create_question(question_text="Future Question.", days = 30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_question_and_past_question(self):
        # Both past and future questions are present but only past are displayed
        create_question(question_text="Past Question.", days = -30)
        create_question(question_text="Future Question.", days = 30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past Question.>'])

    def test_two_past_questions(self):
        # Two past questions must be displayed
        create_question(question_text="Past Question 1.", days = -30)
        create_question(question_text="Past Question 2." ,days = -5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past Question 2.>', '<Question: Past Question 1.>'])

## Tests related to Details view
class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        # Detail of a question with pub_date in the future raises 404 error
        future_question = create_question(question_text = "Future Question.", days = 30)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        # Detail of a past question must be displayed
        past_question = create_question(question_text = "Past Question.", days = -30)
        url = reverse('polls:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
