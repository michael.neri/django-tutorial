from django.urls import path

from . import views
################
# Here we map the views defined in views.py into URLs
#
#       StepGuide
#
#       If we want to enter .../x/y
#       Then we have to define
#           1) a view which we call P for instance in urls.py inside folder "x"
#           2) Define below an url pattern with command path and parameters
#               path('y', views.P, name='Something you want'[optional])
#           3) Finish!!
#       N.B: It is best practiced to name API in order to call them in the Htmls by their name and not by their path

app_name = 'polls'
# NEW VERSION WITH GENERIC views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    # We use the function name to call with 'as_viw()' command
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # Last one does not change
    path('<int:question_id>/vote/' , views.vote, name='vote'),
    # we use pk because DetailView expects the keyname pk
]

# Old Version
#urlpatterns = [
    # ex. /polls/
#    path('', views.index, name='index'),
    # ex. /polls/fool
#    path('lol', views.fool, name='optionalField'),
    # ex. /polls/1/
    # We use <int:question_id> in order to pass parameters to function of the view
#    path('<int:question_id>/', views.detail, name='detail'),
    # ex. /polls/5/results/
#    path('<int:question_id>/results/', views.results, name='results'),
    # ex. /polss/5/vote/
#    path('<int:question_id>/vote/' , views.vote, name='vote'),
#]
