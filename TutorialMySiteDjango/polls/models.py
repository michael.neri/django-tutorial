from django.db import models
from django.utils import timezone
import datetime
# Create your models here.

# Question is an object of the database which has two fields:
#   - Text of the QUESTION
#   - Pubblication Date
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    # Here we write some funnctions of the object Question


    def __str__(self):
        return self.question_text

    # Here we fixed for future question bug
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    # These values are used in order to sort response from arbitraty commands
    # And to display correctly in the admin panel (maybe also in site?)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published Recently?'



# Choice is an object of the database which has three fields:
#   - Question ID (Foreign Key)
#   - Choice text
#   - # of votes
class Choice(models.Model):
    question =  models.ForeignKey(Question,on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default = 0)

    def __str__(self):
        return self.choice_text
