from django.apps import AppConfig

# This is useful for intregrating the poll app into setting.py in INSTALLEDAPS section
class PollsConfig(AppConfig):
    name = 'polls'
